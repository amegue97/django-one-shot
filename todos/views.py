from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from todos.models import TodoList, TodoItem 

from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView

class TodoListView(ListView):
    model = TodoList
    context_object_name = "todo_lists"
    template_name = "todolist/list.html"

# def todo_list_detail(request, pk):
#     model = TodoList
#     todo_list = TodoList.objects.get(pk=pk)
#     context = {
#         "todo_list":todo_list,
#     }
#     return render(request, "todolist/detail.html", context)

class TodoListDetailView(DetailView):
    model = TodoList
    context_object_name = "todo_list"
    template_name = "todolist/detail.html"


# class TodoListCreateView(CreateView):
#     model = TodoList
#     fields = ["name"]
#     template_name = "todolist/create.html"
    
#     def get_success_url(self) -> str:
#         return reverse_lazy("todo_list_detail", args=[self.object.id]) 

# def create_todo_list(request):
#     if request.method == "POST" and TodoListForm:
#         form = TodoListForm(request.POST)
#         if form.is_valid():
#             list = form.save()
#             return redirect("todo_list_detail", pk=list.pk)  
#     elif TodoListForm:
#         form = TodoListForm()
#     else:
#         form = None            
#     context = {
#         "list_form": form,
#     }
#     return render(request, "todolist/create.html", context)        


# def change_todo_list(request, pk):
#     if TodoListForm and TodoList:
#         instance = TodoList.objects.get(pk=pk)
#         if request.method == "POST":
#             form = TodoListForm(request.POST, instance=instance)
#             if form.is_valid():
#                 form.save()
#                 return redirect("todo_list_detail", pk=pk)
#         else:
#             form = TodoListForm(instance=instance)
#     else:
#         form = None
#     context = {
#         "form": form,
#     }                
#     return render(request, "todolist/update.html", context)    

class TodoListCreateView(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "todolist/create.html"

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.id])   


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todolist/update.html"

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView): 
    model = TodoList   
    template_name = "todolist/delete.html"
    success_url = reverse_lazy("todo_list_list")

class TodoItemCreateView(CreateView):
    model = TodoItem 
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todoitem/create.html"
    
    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]  
    template_name = "todoitem/update.html"

    def get_success_url(self) -> str:
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
